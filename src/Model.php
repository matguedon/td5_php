<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                    );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                    );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));

        $books = $this->getBooks();
        $last = count($books)-1;
        $id = $books[$last]['id'];

        for($i=0; $i<$copies; $i++){
            $query2 = $this->pdo->prepare('INSERT INTO exemplaires (book_id)
                VALUES (?)');
            $this->execute($query2, array($id));
        }
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting a book by id
     */
    public function getBook($id){
        $query = $this->pdo->prepare('SELECT livres.* FROM livres WHERE id = :id');

        $this->execute($query, array('id' => $id));

        return $query->fetch();
    }

    public function getExemplaire($id){
        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires WHERE id = :id');

        $this->execute($query, array('id' => $id));

        return $query->fetch();
    }

    public function getExemplairesByBook($id)
    {
        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires WHERE book_id = :id');

        $this->execute($query, array('id' => $id));

        return $query->fetchAll();
    }

    public function getExemplairesDispoByBook($id)
    {
        $exemplaires = $this->getExemplairesByBook($id);
        $exemplairesDispo = $exemplaires;

        foreach ($exemplaires as $exemplaire) {
            $empruntEnCours = $this->getEmpruntNotFinishedByExemplaire($exemplaire['id']);
            if($empruntEnCours != false){
                $key = array_search($exemplaire, $exemplaires);
                unset($exemplairesDispo[$key]);
            }
        }

        return $exemplairesDispo;
    }

    public function insertEmprunt($personne, $exemplaire_id, $fin)
    {
        $query = $this->pdo->prepare('INSERT INTO emprunts (personne, exemplaire, debut, fin, fini)
            VALUES (?, ?, ?, ?, ?)');
        $this->execute($query, array($personne, $exemplaire_id, (new DateTime())->format('Y-m-d H:i:s'), $fin->format('Y-m-d H:i:s'), false));
    }

    public function getEmpruntsByExemplaire($id){
       $query = $this->pdo->prepare('SELECT emprunts.* FROM emprunts WHERE exemplaire = :id');

       $this->execute($query, array('id' => $id));

       return $query->fetchAll();   
   }

    public function getEmpruntNotFinishedByExemplaire($id){
        $query = $this->pdo->prepare('SELECT emprunts.* FROM emprunts WHERE exemplaire = ? AND fini = ?');
        $this->execute($query, array($id, false));

        return $query->fetch();
    }

   public function updateEmprunt($id){
        $query = $this->pdo->prepare('UPDATE emprunts SET fin = ?, fini=? WHERE id = ?');
        $this->execute($query, array((new DateTime())->format('y-m-d H:i:s'), true, $id));
   }
}
