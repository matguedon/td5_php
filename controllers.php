<?php

use Gregwar\Image\Image;

$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

$app->match('/books', function() use ($app) {
    return $app['twig']->render('books.html.twig', array(
        'books' => $app['model']->getBooks()
    ));
})->bind('books');

$app->match('/book/{id}', function($id) use ($app) {
    $exemplaires = $app['model']->getExemplairesByBook($id);
    $exemplairesDispo = $app['model']->getExemplairesDispoByBook($id);

    $exemplairesIndispo = $exemplaires;
    foreach ($exemplairesDispo as $exemplaire) {
        $key = array_search($exemplaire, $exemplaires);
        unset($exemplairesIndispo[$key]);
    }

    return $app['twig']->render('showBook.html.twig', array(
        'book' => $app['model']->getBook($id),
        'exemplaires' => $exemplaires,
        'exemplairesDispo' => $exemplairesDispo,
        'exemplairesIndispo' => $exemplairesIndispo
    ));
})->bind('book');

$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password') &&
            in_array(array($post->get('login'), $post->get('password')), $app['config']['admin']) ) {
            $app['session']->set('admin', true);
            $success = true;
        }
    }
    return $app['twig']->render('admin.html.twig', array(
        'success' => $success
    ));
})->bind('admin');

$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('logout');

$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
        }
    }

    return $app['twig']->render('addBook.html.twig');
})->bind('addBook');

$app->match('/emprunter/{id}', function($id) use ($app) {
    $exemplaire = $app['model']->getExemplaire($id);
    $bookId = $exemplaire['book_id'];

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('name') && $post->has('day') && $post->has('month') && $post->has('year')) {
            $dateDebut = new DateTime();
            $d = $post->get('day');
            $m = $post->get('month');
            $y = $post->get('year');
            $dateFin = new DateTime($y . '-' . $m . '-' . $d . '00:00:00');
            
            if( $dateFin->getTimestamp() - $dateDebut->getTimestamp() > 0)
                $app['model']->insertEmprunt($post->get('name'), $id, $dateFin);
            else
                echo "La date de fin doit être supérieure à la date courante";

            return $app->redirect($app['url_generator']->generate('book', array("id" => $bookId)));
        }
    }
    return $app['twig']->render('emprunter.html.twig');
})->bind('emprunter');

$app->match('/retourner/{id}', function($id) use ($app) {
    $exemplaire = $app['model']->getExemplaire($id);
    $bookId = $exemplaire['book_id'];

    $emprunt = $app['model']->getEmpruntNotFinishedByExemplaire($id);

    $app['model']->updateEmprunt($emprunt['id']);

    return $app->redirect($app['url_generator']->generate('book', array("id" => $bookId)));
})->bind('retourner');