<?php

class SiteTests extends BaseTests
{
    /**
     * Testing that accessing the secure page doesn't works, and then logging
     */
    public function testAdmin()
    {
        $client = $this->createClient();

        // We are not admin, check that we get the error
        $crawler = $client->request('GET', '/addBook');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('.shouldbeadmin'));

        // Logging in as admin, bad password
        $crawler = $client->request('POST', '/admin', ['login' => 'admin', 'password' => 'bad']);
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(0, $crawler->filter('.loginsuccess'));

        // Logging in as admin, success
        $crawler = $client->request('POST', '/admin', ['login' => 'admin1', 'password' => 'password1']);
        $this->assertCount(1, $crawler->filter('.loginsuccess'));

        // Now, we should get the page
        $crawler = $client->request('GET', '/addBook');
        $this->assertCount(0, $crawler->filter('.shouldbeadmin'));

        // Disconnect
        $crawler = $client->request('GET', '/logout');
        $this->assertTrue($client->getResponse()->isRedirect());
        $crawler = $client->request('GET', '/addBook');
        $this->assertCount(1, $crawler->filter('.shouldbeadmin'));
    }

    /**
     * Testing book insert (using form)
     */
    public function testBookInsertForm()
    {
        $client = $this->createClient();
        $this->app['session']->set('admin', true);

        // There is no book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(0, count($books));

        // Inserting one using a POST request through the form
        $client->request('GET', '/addBook');
        $form = $client->getCrawler()->filter('form')->form();
        $form['title'] = 'Test';
        $form['author'] = 'Someone';
        $form['synopsis'] = 'A test book';
        $form['copies'] = 3;
        $client->submit($form);

        // There is one book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(1, count($books));
    }

    public function testEmprunt(){
        $client = $this->createClient();
        $this->app['session']->set('admin', true);

        // Insertion d'un livre et de ses 3 exemplaires
        $synopsis = "Avant l’accident, Ever Bloom était une adolescente populaire et joyeuse. 
                    Quand elle perd toute sa famille dans cet épisode tragique, elle reçoit soudain 
                    un terrible don : celui de lire dans les pensées des gens et de connaître leur vie 
                    simplement en les touchant. Elle se renferme alors sur elle-même et évite le contact 
                    des élèves du lycée qui la regardent comme une chose bizarre… Jusqu’au jour où elle 
                    rencontre Damen Auguste. Damen est mystérieux et d’une beauté inquiétante. 
                    Toutes les filles du lycée se le disputent mais c’est à Ever qu’il s’intéresse. 
                    Or c’est le seul être dont elle ne peut pas découvrir les pensées. Et personne ne 
                    sait réellement qui il est ni d’où il vient. Damen n’est pas un simple mortel, 
                    Ever en est certaine. Elle sait aussi qu’elle est profondément et 
                    irrémédiablement attirée par lui.";

        $this->app['model']->insertBook("Eternels Tome 1", "Alyson Noël", $synopsis, "12bf81b00a86ea9ae3f54ccd54dadd4f44409c39", 3);

        // Il y a 3 exemplaires disponibles de ce livre
        $exemplairesDispo = $this->app['model']->getExemplairesDispoByBook(1);
        $this->assertEquals(3, count($exemplairesDispo));

        //Emprunt d'un exemplaire
        $client->request('GET', '/emprunter/1');
        $form = $client->getCrawler()->filter('form')->form();
        $form['name'] = 'Dupond';
        $form['day'] = 12;
        $form['month'] = 02;
        $form['year'] = 2016;
        $client->submit($form);

        //Il y a 2 exemplaires disponibles
        $exemplairesDispo = $this->app['model']->getExemplairesDispoByBook(1);
        $this->assertEquals(2, count($exemplairesDispo));

        //Retour
        $client->request('GET', '/retourner/1');

        //Il y a 3 exemplaires disponibles
        $exemplairesDispo = $this->app['model']->getExemplairesDispoByBook(1);
        $this->assertEquals(3, count($exemplairesDispo));

    }
}
